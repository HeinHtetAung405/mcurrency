import 'package:flutter/material.dart';

class NumberLabelTextWidget extends StatelessWidget {
  NumberLabelTextWidget({@required this.label, this.onClear});

  final String label;
  final Function onClear;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClear,
      child: Image.asset(
        label,
        width: 50.0,
        height: 30.0,
        color: Colors.white,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class WaveLayerWidget extends StatelessWidget {
  const WaveLayerWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 152,
      width: double.infinity,
      child: WaveWidget(
        config: CustomConfig(
          colors: [
            Colors.lightBlue[100],
            Colors.lightBlue[200],
            Colors.lightBlue[300],
            Colors.lightBlue[400]
          ],
          durations: [35000, 19440, 10800, 6000],
          heightPercentages: [0.20, 0.23, 0.25, 0.30],
        ),
        backgroundColor: Colors.transparent,
        size: Size(double.infinity, double.infinity),
        waveAmplitude: 1.0,
        waveFrequency: 1.0,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:mcurrency/model/countryCurrency.dart';
import 'package:mcurrency/model/currencyItem.dart';
import 'package:mcurrency/utils/constants.dart';
import 'package:mcurrency/utils/navigator.dart';

class CurrencyPage extends StatefulWidget {
  @override
  _CurrencyPageState createState() => _CurrencyPageState();
}

class _CurrencyPageState extends State<CurrencyPage> {
  List<CurrencyItem> currencyList = List();

  @override
  void initState() {
    super.initState();

    currencyList = TempStore().currencyList;
  }

  @override
  Widget build(BuildContext context) {
    void _showConvertCurrencySheet(String currency, String price) {
      String changeFormat = price.replaceAll(',', '');
      double value = double.parse(changeFormat);
      int getValue = value.toInt();
      showModalBottomSheet<void>(
          context: context,
          builder: (BuildContext context) {
            return Container(
              padding: EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('1 $currency', style: kCurrencyConvertTextStyle),
                  Image.asset(
                    'assets/images/convert.png',
                    width: 30.0,
                    height: 30.0,
                  ),
                  Text('$getValue Kyats', style: kCurrencyConvertTextStyle),
                ],
              ),
            );
          });
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('Currency'),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.info_outline,
                color: Colors.white,
              ),
              onPressed: () => goToAboutPage(context),
            )
          ],
        ),
        body: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: currencyList.length,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              final CurrencyItem currencyItem = currencyList[index];
              return ListTile(
                leading: Image.asset(currencyItem.countryFlag),
                title: Text(currencyItem.label),
                subtitle: Text(currencyItem.value + " Kyats"),
                onTap: () => _showConvertCurrencySheet(
                    currencyItem.label, currencyItem.value),
              );
            }));
  }
}
